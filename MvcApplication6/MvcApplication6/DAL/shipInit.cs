﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using test1.Models;
using test1.DAL;

namespace test1.DAL
{
    public class shipInit : DropCreateDatabaseIfModelChanges<shipContext>
    {
        protected override void Seed(shipContext context)
        {
            var drivers = new List<Drivers>
            {
                new Drivers { truckID = 1, cargoID = 2, FirstName="Doug" , LastName="StrongArm"},
                new Drivers {  truckID = 2, cargoID = 1, FirstName="Chuck" , LastName="HeartPunch"}

            };
            drivers.ForEach(s => context.Drivers.Add(s));
            context.SaveChanges();

            var cargoes = new List<Cargo>
            {
                new Cargo{ locationID=1,cargoDescription="lungfish",cargoType="Living" },
                new Cargo{ locationID=2,cargoDescription="vinyl records",cargoType="Normal" },
                new Cargo{ locationID=3,cargoDescription="Fuel",cargoType="Combustable" }
            };
            cargoes.ForEach(s => context.Cargo.Add(s));
            context.SaveChanges();

            var locs = new List<Locations>
            {
                new Locations { cargoID=1 ,address="742 Evergreen Terrace Springfield Oregon"},
                new Locations { cargoID=2 ,address="2910 Elm St. Pittsburgh Trannsylvania"},
                new Locations { cargoID=3 ,address="The Bat Cave"}
                
                
            };
            locs.ForEach(s => context.Locations.Add(s));
            context.SaveChanges();

            var truckus = new List<Trucks>
            {
                new Trucks { CargoType="fuel",driverID=1},
                new Trucks { CargoType="lungfish",driverID=2},
                 // new Trucks { cargoID=19 ,locationID=414},
                
            };
            truckus.ForEach(s => context.Trucks.Add(s));
            context.SaveChanges();


        }
    }
}