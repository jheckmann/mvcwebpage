﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using test1.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace test1.Models
{
    public class shipContext : DbContext
    {
        public DbSet<Trucks> Trucks { get; set; }
        public DbSet<Drivers> Drivers { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DbSet<Cargo> Cargo { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }



    }
}