var HT = HT || {};
/**
 * A Point is simply x and y coordinates
 * @constructor
 */
HT.Point = function(x, y) {
	this.X = x;
	this.Y = y;
};

/**
 * A Rectangle is x and y origin and width and height
 * @constructor
 */
HT.Rectangle = function(x, y, width, height) {
	this.X = x;
	this.Y = y;
	this.Width = width;
	this.Height = height;
};

/**
 * A Line is x and y start and x and y end
 * @constructor
 */
HT.Line = function(x1, y1, x2, y2) {
	this.X1 = x1;
	this.Y1 = y1;
	this.X2 = x2;
	this.Y2 = y2;
};

/**
 * A Hexagon is a 6 sided polygon, our hexes don't have to be symmetrical, i.e. ratio of width to height could be 4 to 3
 * @constructor
 */
HT.Hexagon = function(id, x, y) {
	this.Points = [];//Polygon Base
	var x1 = null;
	var y1 = null;
	if(HT.Hexagon.Static.ORIENTATION == HT.Hexagon.Orientation.Normal) {
		x1 = (HT.Hexagon.Static.WIDTH - HT.Hexagon.Static.SIDE)/2;
		y1 = (HT.Hexagon.Static.HEIGHT / 2);
		this.Points.push(new HT.Point(x1 + x, y));
		this.Points.push(new HT.Point(x1 + HT.Hexagon.Static.SIDE + x, y));
		this.Points.push(new HT.Point(HT.Hexagon.Static.WIDTH + x, y1 + y));
		this.Points.push(new HT.Point(x1 + HT.Hexagon.Static.SIDE + x, HT.Hexagon.Static.HEIGHT + y));
		this.Points.push(new HT.Point(x1 + x, HT.Hexagon.Static.HEIGHT + y));
		this.Points.push(new HT.Point(x, y1 + y));
	}
	else {
		x1 = (HT.Hexagon.Static.WIDTH / 2);
		y1 = (HT.Hexagon.Static.HEIGHT - HT.Hexagon.Static.SIDE)/2;
		this.Points.push(new HT.Point(x1 + x, y));
		this.Points.push(new HT.Point(HT.Hexagon.Static.WIDTH + x, y1 + y));
		this.Points.push(new HT.Point(HT.Hexagon.Static.WIDTH + x, y1 + HT.Hexagon.Static.SIDE + y));
		this.Points.push(new HT.Point(x1 + x, HT.Hexagon.Static.HEIGHT + y));
		this.Points.push(new HT.Point(x, y1 + HT.Hexagon.Static.SIDE + y));
		this.Points.push(new HT.Point(x, y1 + y));
	}
	
	this.Id = id;
	
	this.x = x;
	this.y = y;
	this.x1 = x1;
	this.y1 = y1;
	
	this.TopLeftPoint = new HT.Point(this.x, this.y);
	this.BottomRightPoint = new HT.Point(this.x + HT.Hexagon.Static.WIDTH, this.y + HT.Hexagon.Static.HEIGHT);
	this.MidPoint = new HT.Point(this.x + (HT.Hexagon.Static.WIDTH / 2), this.y + (HT.Hexagon.Static.HEIGHT / 2));
	
	this.P1 = new HT.Point(x + x1, y + y1);
	
	this.selected = false;
};
	
/**
 * draws this Hexagon to the canvas
 * @this {HT.Hexagon}
 */
HT.Hexagon.prototype.draw = function(ctx) {

	if(!this.selected)
		ctx.strokeStyle = "grey";
	else
		ctx.strokeStyle = "black";
	ctx.lineWidth = 1;
	ctx.beginPath();
	ctx.moveTo(this.Points[0].X, this.Points[0].Y);
	for(var i = 1; i < this.Points.length; i++)
	{
		var p = this.Points[i];
		ctx.lineTo(p.X, p.Y);
	}
	ctx.closePath();
	ctx.stroke();
	
	if(this.Id)
	{
		//draw text for debugging
		ctx.fillStyle = "black"
		ctx.font = "bolder 8pt Trebuchet MS,Tahoma,Verdana,Arial,sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = 'middle';
		//var textWidth = ctx.measureText(this.Planet.BoundingHex.Id);
		ctx.fillText(this.Id, this.MidPoint.X, this.MidPoint.Y);
	}
	
	if(this.PathCoOrdX !== null && this.PathCoOrdY !== null && typeof(this.PathCoOrdX) != "undefined" && typeof(this.PathCoOrdY) != "undefined")
	{
		//draw co-ordinates for debugging
		ctx.fillStyle = "black"
		ctx.font = "bolder 8pt Trebuchet MS,Tahoma,Verdana,Arial,sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = 'middle';
		//var textWidth = ctx.measureText(this.Planet.BoundingHex.Id);
		ctx.fillText("("+this.PathCoOrdX+","+this.PathCoOrdY+")", this.MidPoint.X, this.MidPoint.Y + 10);
	}
	
	if(HT.Hexagon.Static.DRAWSTATS)
	{
		ctx.strokeStyle = "black";
		ctx.lineWidth = 2;
		//draw our x1, y1, and z
		ctx.beginPath();
		ctx.moveTo(this.P1.X, this.y);
		ctx.lineTo(this.P1.X, this.P1.Y);
		ctx.lineTo(this.x, this.P1.Y);
		ctx.closePath();
		ctx.stroke();
		
		ctx.fillStyle = "black"
		ctx.font = "bolder 8pt Trebuchet MS,Tahoma,Verdana,Arial,sans-serif";
		ctx.textAlign = "left";
		ctx.textBaseline = 'middle';
		//var textWidth = ctx.measureText(this.Planet.BoundingHex.Id);
		ctx.fillText("z", this.x + this.x1/2 - 8, this.y + this.y1/2);
		ctx.fillText("x", this.x + this.x1/2, this.P1.Y + 10);
		ctx.fillText("y", this.P1.X + 2, this.y + this.y1/2);
		ctx.fillText("z = " + HT.Hexagon.Static.SIDE, this.P1.X, this.P1.Y + this.y1 + 10);
		ctx.fillText("(" + this.x1.toFixed(2) + "," + this.y1.toFixed(2) + ")", this.P1.X, this.P1.Y + 10);
	}
};
/**
 * draws this Hexagon using SVG
 * @this {HT.Hexagon}
 */
function colorSwitch(hex)
{
	if((hex.getAttribute("fill")=='white') || (hex.getAttribute("fill")=='url(#rg3)'))
	{
	
	//hex.attribute({'fill':'url(#grad1)'});
	//hex.setAttribute('fill','url(#grad1)');
	hex.setAttribute('fill','url(#rg1)');
	}
	else if(hex.getAttribute("fill")=='url(#rg1)')
	{
	hex.setAttribute('fill','url(#rg2)');

	}
	else
	{
	hex.setAttribute('fill','url(#rg3)');
	
	//	hex.setAttribute('fill','#00FFFF');
	}
}
/**
 * draws this Hexagon using SVG
 * @this {HT.Hexagon}
 */
 HT.Hexagon.prototype.drawSVG = function(mySvg) {

	if(!this.selected)
	{	
		//mySvg.setAttribute("stroke",'FF00FF');
		mySvg.setAttribute("stroke",'black');
	}
	else 
	{
		//mySvg.setAttribute("stroke",'#FF00FF');
		mySvg.setAttribute("stroke",'black');
	}

	var pString ="M"+this.Points[0].X+" "+this.Points[0].Y;
	var polyLine=this.Points[0].X+","+this.Points[0].Y+" ";
	for(var i = 1; i < this.Points.length; i++)
	{
		var p = this.Points[i];
		pString=pString + " L "+p.X+" "+p.Y;
		polyLine=polyLine+p.X+","+p.Y+" ";
	}
	polyLine=polyLine+this.Points[0].X+","+this.Points[0].Y+" ";
	pString=pString+ " z"
	p1= document.createElementNS('http://www.w3.org/2000/svg', 'path');
	//p1.setAttribute('stroke', '#FF00FF'); 
	p1.setAttribute('stroke','black');
	p1.setAttribute("d",pString);
	p2= document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
	//p2.setAttribute('stroke','#FF00FF');
	p2.setAttribute('stroke','white');
	p2.setAttribute('points',polyLine);
	p2.setAttribute('fill','white');
	
	//pink gradient
	var radGrad1=document.createElementNS('http://www.w3.org/2000/svg', 'radialGradient');
    radGrad1.id="rg1";
	mySvg.appendChild(radGrad1);
	
	//yellow gradient
	var radGrad2=document.createElementNS('http://www.w3.org/2000/svg', 'radialGradient');
    radGrad2.id="rg2";
	mySvg.appendChild(radGrad2);
	
	//green gradient
	var radGrad3=document.createElementNS('http://www.w3.org/2000/svg', 'radialGradient');
    radGrad3.id="rg3";
	mySvg.appendChild(radGrad3);

	////////////////////////////////phase 1
	var gradientStart1 = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStart1.setAttribute('offset', '0%');
    gradientStart1.setAttribute('stop-color', 'Grey');
    radGrad1.appendChild(gradientStart1);
	
	var gradientStart1A = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStart1A.setAttribute('offset', '25%');
    gradientStart1A.setAttribute('stop-color', 'DarkGrey');
    radGrad1.appendChild(gradientStart1A);
	
	var gradientStop1 = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStop1.setAttribute('offset', '100%');
    gradientStop1.setAttribute('stop-color', 'Grey');
	radGrad1.appendChild(gradientStop1);
//move the inner ring
	var animateStop1 = document.createElementNS('http://www.w3.org/2000/svg', 'animate');
    animateStop1.setAttributeNS(null,'attributeName', 'offset');
	animateStop1.setAttributeNS(null,'values', '0;1');
	animateStop1.setAttributeNS(null,'begin', '0s');
	animateStop1.setAttributeNS(null,'dur', "3s");
	animateStop1.setAttributeNS(null,'repeatCount', 'indefinite');
	gradientStart1A.appendChild(animateStop1);


//changes outer colors
	var animateStop1A = document.createElementNS('http://www.w3.org/2000/svg', 'animate');
    animateStop1A.setAttributeNS(null,'attributeName', 'stop-color');
	animateStop1A.setAttributeNS(null,'values', 'grey;DarkGrey;');
	animateStop1A.setAttributeNS(null,'begin', '0s');
	animateStop1A.setAttributeNS(null,'dur', "6s");
	animateStop1A.setAttributeNS(null,'fill', 'freeze');
	animateStop1A.setAttributeNS(null,'repeatCount', 'indefinite');
	gradientStop1.appendChild(animateStop1A);
/////////////////////////////////////////////	
//phase2/white
	//inner pink ring of phase 2
	var gradientStart2 = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStart2.setAttribute('offset', '0%');
    gradientStart2.setAttribute('stop-color', 'darkGrey');
	radGrad2.appendChild(gradientStart2);
	//outer blue ring of phase 2
	var gradientStart2A = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStart2A.setAttribute('offset', '25%');
	gradientStart2A.setAttribute('stop-color', 'grey');
	radGrad2.appendChild(gradientStart2A);
	//pink and blue
	var gradientStop2 = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStop2.setAttribute('offset', '100%');
    gradientStop2.setAttribute('stop-color', 'darkgrey');
	radGrad2.appendChild(gradientStop2);
	//move the inner ring
	var animateStop2 = document.createElementNS('http://www.w3.org/2000/svg', 'animate');
    animateStop2.setAttributeNS(null,'attributeName', 'offset');
	animateStop2.setAttributeNS(null,'values', '0;1');
	animateStop2.setAttributeNS(null,'begin', '0s');
	animateStop2.setAttributeNS(null,'dur', "3s");
	animateStop2.setAttributeNS(null,'fill', 'freeze');
	animateStop2.setAttributeNS(null,'repeatCount', 'indefinite');
	gradientStart2A.appendChild(animateStop2);

	//moves the outer ring
	var animateStop2A = document.createElementNS('http://www.w3.org/2000/svg', 'animate');
    animateStop2A.setAttributeNS(null,'attributeName', 'stop-color');
	animateStop2A.setAttributeNS(null,'values', 'darkgrey;grey;');
	animateStop2A.setAttributeNS(null,'begin', '0s');
	animateStop2A.setAttributeNS(null,'dur', "3s");
	animateStop2A.setAttributeNS(null,'fill', 'freeze');
	animateStop2A.setAttributeNS(null,'repeatCount', 'indefinite');
	gradientStop2.appendChild(animateStop2A);
	
	
	var animateStop2B = document.createElementNS('http://www.w3.org/2000/svg', 'animate');
    animateStop2B.setAttributeNS(null,'attributeName', 'stop-color');
	animateStop2B.setAttributeNS(null,'values', 'grey;white');
	animateStop2B.setAttributeNS(null,'begin', '0s');
	animateStop2B.setAttributeNS(null,'dur', "3s");
	animateStop2B.setAttributeNS(null,'fill', 'freeze');
	animateStop2B.setAttributeNS(null,'repeatCount', 'indefinite');
	//gradientStart2A.appendChild(animateStop2B);
	
	
/////////////////////////////////////////////	
//phase3	
	//inner  ring of phase 3
	var gradientStart4 = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStart4.setAttribute('offset', '0%');
	gradientStart4.setAttribute('stop-color', 'darkgrey');
	radGrad3.appendChild(gradientStart4);
	//second inner  ring of phase 3
	var gradientStart3 = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStart3.setAttribute('offset', '25%');
	gradientStart3.setAttribute('stop-color', 'white');
	radGrad3.appendChild(gradientStart3);
	//stop of phase 3
	var gradientStop3 = document.createElementNS('http://www.w3.org/2000/svg', 'stop');
    gradientStop3.setAttribute('offset', '100%');
	gradientStop3.setAttribute('stop-color', 'darkgrey');
	radGrad3.appendChild(gradientStop3);
	//// move gradiant 
	var animateStop3 = document.createElementNS('http://www.w3.org/2000/svg', 'animate');
    animateStop3.setAttributeNS(null,'attributeName', 'offset');
	animateStop3.setAttributeNS(null,'values', '0;1');
	animateStop3.setAttributeNS(null,'begin', '0s');
	animateStop3.setAttributeNS(null,'dur', "3s");
	animateStop3.setAttributeNS(null,'fill', 'freeze');
	animateStop3.setAttributeNS(null,'repeatCount', 'indefinite');
	gradientStart3.appendChild(animateStop3);
	//color change
	var animateStop3A = document.createElementNS('http://www.w3.org/2000/svg', 'animate');
    animateStop3A.setAttributeNS(null,'attributeName', 'stop-color');
	animateStop3A.setAttributeNS(null,'values', 'darkgrey;white');
	animateStop3A.setAttributeNS(null,'begin', '0s');
	animateStop3A.setAttributeNS(null,'dur', "3s");
	animateStop3A.setAttributeNS(null,'fill', 'freeze');
	animateStop3A.setAttributeNS(null,'repeatCount', 'indefinite');
	gradientStop3.appendChild(animateStop3A);
/////////////////////////////////////////////	
	

	mySvg.appendChild(p2);
	p2.setAttribute('onclick',"colorSwitch(this)");
/*	
	if(this.Id)
	{
		//draw text for debugging
		mySVG.fillStyle = "black"
		mySVG.font = "bolder 8pt Trebuchet MS,Tahoma,Verdana,Arial,sans-serif";
		mySVG.textAlign = "center";
		mySVG.textBaseline = 'middle';
		//var textWidth = ctx.measureText(this.Planet.BoundingHex.Id);
		mySVG.fillText(this.Id, this.MidPoint.X, this.MidPoint.Y);
	}
	
	if(this.PathCoOrdX !== null && this.PathCoOrdY !== null && typeof(this.PathCoOrdX) != "undefined" && typeof(this.PathCoOrdY) != "undefined")
	{
		//draw co-ordinates for debugging
		mySVG.fillStyle = "black"
		mySVG.font = "bolder 8pt Trebuchet MS,Tahoma,Verdana,Arial,sans-serif";
		mySVG.textAlign = "center";
		mySVG.textBaseline = 'middle';
		//var textWidth = ctx.measureText(this.Planet.BoundingHex.Id);
		mySVG.fillText("("+this.PathCoOrdX+","+this.PathCoOrdY+")", this.MidPoint.X, this.MidPoint.Y + 10);
	}
	
	if(HT.Hexagon.Static.DRAWSTATS)
	{
		mySVG.strokeStyle = "black";
		mySVG.lineWidth = 2;
		//draw our x1, y1, and z
		mySVG.beginPath();
		mySVG.moveTo(this.P1.X, this.y);
		mySVG.lineTo(this.P1.X, this.P1.Y);
		mySVG.lineTo(this.x, this.P1.Y);
		mySVG.closePath();
		mySVG.stroke();
		
		mySVG.fillStyle = "black"
		mySVG.font = "bolder 8pt Trebuchet MS,Tahoma,Verdana,Arial,sans-serif";
		mySVG.textAlign = "left";
		mySVG.textBaseline = 'middle';
		//var textWidth = ctx.measureText(this.Planet.BoundingHex.Id);
		mySVG.fillText("z", this.x + this.x1/2 - 8, this.y + this.y1/2);
		mySVG.fillText("x", this.x + this.x1/2, this.P1.Y + 10);
		mySVG.fillText("y", this.P1.X + 2, this.y + this.y1/2);
		mySVG.fillText("z = " + HT.Hexagon.Static.SIDE, this.P1.X, this.P1.Y + this.y1 + 10);
		mySVG.fillText("(" + this.x1.toFixed(2) + "," + this.y1.toFixed(2) + ")", this.P1.X, this.P1.Y + 10);
	}*/
};//~~~~//!!!!!//~~~


/**
 * Returns true if the x,y coordinates are inside this hexagon
 * @this {HT.Hexagon}
 * @return {boolean}
 */
HT.Hexagon.prototype.isInBounds = function(x, y) {
	return this.Contains(new HT.Point(x, y));
};
	

/**
 * Returns true if the point is inside this hexagon, it is a quick contains
 * @this {HT.Hexagon}
 * @param {HT.Point} p the test point
 * @return {boolean}
 */
HT.Hexagon.prototype.isInHexBounds = function(/*Point*/ p) {
	if(this.TopLeftPoint.X < p.X && this.TopLeftPoint.Y < p.Y &&
	   p.X < this.BottomRightPoint.X && p.Y < this.BottomRightPoint.Y)
		return true;
	return false;
};

//grabbed from:
//http://www.developingfor.net/c-20/testing-to-see-if-a-point-is-within-a-polygon.html
//and
//http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html#The%20C%20Code
/**
 * Returns true if the point is inside this hexagon, it first uses the quick isInHexBounds contains, then check the boundaries
 * @this {HT.Hexagon}
 * @param {HT.Point} p the test point
 * @return {boolean}
 */
HT.Hexagon.prototype.Contains = function(/*Point*/ p) {
	var isIn = false;
	if (this.isInHexBounds(p))
	{
		//turn our absolute point into a relative point for comparing with the polygon's points
		//var pRel = new HT.Point(p.X - this.x, p.Y - this.y);
		var i, j = 0;
		for (i = 0, j = this.Points.length - 1; i < this.Points.length; j = i++)
		{
			var iP = this.Points[i];
			var jP = this.Points[j];
			if (
				(
				 ((iP.Y <= p.Y) && (p.Y < jP.Y)) ||
				 ((jP.Y <= p.Y) && (p.Y < iP.Y))
				//((iP.Y > p.Y) != (jP.Y > p.Y))
				) &&
				(p.X < (jP.X - iP.X) * (p.Y - iP.Y) / (jP.Y - iP.Y) + iP.X)
			   )
			{
				isIn = !isIn;
			}
		}
	}
	return isIn;
};


HT.Hexagon.Orientation = {
	Normal: 0,
	Rotated: 1
};

HT.Hexagon.Static = {HEIGHT:91.14378277661477
					, WIDTH:91.14378277661477
					, SIDE:50.0
					, ORIENTATION:HT.Hexagon.Orientation.Normal
					, DRAWSTATS: false};//hexagons will have 25 unit sides for now


