﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace test1.Models
{
    public class Trucks
    {

        [Key]
        public int truckID { get; set; }
        public string CargoType { get; set; }
        public int driverID { get; set; }

    }
}